package isfirs.uniore;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class Registration {

    private static final Logger LOGGER = LogManager.getLogger();

    public static final DeferredRegister<Block> BLOCK_REGISTRY;
    public static final DeferredRegister<Item> ITEM_REGISTRY;

    static {
        BLOCK_REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCKS, UniOre.MODID);
        ITEM_REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, UniOre.MODID);
    }

    private static boolean initialized = false;

    public static synchronized void init() {
        if (initialized) return;

        LOGGER.info("{} Materials initialized", Metals.values().length);

        BLOCK_REGISTRY.register(FMLJavaModLoadingContext.get().getModEventBus());
        ITEM_REGISTRY.register(FMLJavaModLoadingContext.get().getModEventBus());

        initialized = true;
    }

}
