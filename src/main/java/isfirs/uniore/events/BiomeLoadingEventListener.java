package isfirs.uniore.events;

import isfirs.uniore.UniOre;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraftforge.common.world.BiomeGenerationSettingsBuilder;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.ArrayList;
import java.util.List;

@Mod.EventBusSubscriber
public final class BiomeLoadingEventListener {

    private static final List<ConfiguredFeature<?,?>> OVERWORLD_GEN_FEATURES = new ArrayList<>();

    public static void add(ConfiguredFeature<?,?> fc) {
        OVERWORLD_GEN_FEATURES.add(fc);
    }

    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public static void on(BiomeLoadingEvent event) {
        final BiomeGenerationSettingsBuilder generation = event.getGeneration();
        switch (event.getCategory()) {
            case NETHER:
            case THEEND:
                break;
            default:
                OVERWORLD_GEN_FEATURES.forEach(feature -> {
                    generation.withFeature(GenerationStage.Decoration.UNDERGROUND_ORES, feature);
                });
        }
    }

}
