package isfirs.uniore;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;

import java.util.function.Supplier;

final class MaterialUtil {

    //

     static Supplier<Block> buildBlockOre() {
        return () -> {
            final AbstractBlock.Properties props = createDefaultBlockProps();
            return new Block(props);
        };
    }

     static Supplier<Block> buildBlockOreDeepslate() {
        return () -> {
            final AbstractBlock.Properties props = createDefaultBlockProps();
            return new Block(props);
        };
    }

     static Supplier<Block> buildRawBlock() {
        return () -> {
            final AbstractBlock.Properties props = createDefaultBlockProps();
            return new Block(props);
        };
    }

     static Supplier<Block> buildBlock() {
        return () -> {
            final AbstractBlock.Properties props = createDefaultBlockProps();
            return new Block(props);
        };
    }

    // endregion Blocks

    // region Items

     static Supplier<Item> buildItemOreBlock(final Supplier<Block> block) {
        return () -> {
            final Item.Properties props = createDefaultItemProps();
            return new BlockItem(block.get(), props);
        };
    }

     static Supplier<Item> buildItemRaw() {
        return () -> {
            final Item.Properties props = createDefaultItemProps();
            return new Item(props);
        };
    }

     static Supplier<Item> buildItemRawBlock(final Supplier<Block> block) {
        return () -> {
            final Item.Properties props = createDefaultItemProps();
            return new BlockItem(block.get(), props);
        };
    }

     static Supplier<Item> buildItemNugget() {
        return () -> {
            final Item.Properties props = createDefaultItemProps();
            return new Item(props);
        };
    }

     static Supplier<Item> buildItemIngot() {
        return () -> {
            final Item.Properties props = createDefaultItemProps();
            return new Item(props);
        };
    }

     static Supplier<Item> buildItemBlock(final Supplier<Block> block) {
        return () -> {
            final Item.Properties props = createDefaultItemProps();
            return new BlockItem(block.get(), props);
        };
    }

    // endregion Items

    // region Utility

     static String format(final String pattern, final Object...args) {
        return String.format(pattern, args);
    }

     static AbstractBlock.Properties createDefaultBlockProps() {
        return AbstractBlock.Properties.create(Material.ROCK)
              .setRequiresTool()
              .hardnessAndResistance(3f);
    }

     static Item.Properties createDefaultItemProps() {
        return new Item.Properties() //
              .group(ItemGroup.MATERIALS);
    }

    // endregion Utility

     MaterialUtil() {
        throw new IllegalStateException("Utility class can not be instantiated");
    }

}
