package isfirs.uniore;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.world.gen.placement.ConfiguredPlacement;
import net.minecraft.world.gen.placement.Placement;
import net.minecraft.world.gen.placement.TopSolidRangeConfig;
import net.minecraftforge.fml.RegistryObject;

import java.awt.Color;

import static isfirs.uniore.MaterialUtil.*;
import static isfirs.uniore.Registration.BLOCK_REGISTRY;
import static isfirs.uniore.Registration.ITEM_REGISTRY;

public enum Metals {

    Bauxite(
          "Bauxite",
          "Aluminium",
          new Color(219, 140, 103),
          new Color(224, 224, 224),
          8
    ),

    Silver(
          "Silver",
          new Color(161, 161, 161),
          8
    ),

    Tin(
          "Cassiterite",
          "Tin",
          new Color(198, 198, 198),
          8
    ),

    Titanium(
          "Titanium",
          new Color(139, 163, 201),
          8
    ),

    Tungsten(
          "Tungsten",
          new Color(73, 73, 73),
          new Color(99, 99, 99),
          8
    ),

    Zinc(
          "Zinc",
          new Color(165, 159, 159),
          8
    ),
    ;

    public final String oreName;
    public final String metalName;
    public final Color oreTint;
    public final Color metalTint;
    public final int clusterSize;

    public final RegistryObject<Block> BLOCK_ORE;
    public final RegistryObject<Item> ITEM_BLOCK_ORE;

    public final RegistryObject<Block> BLOCK_ORE_DEEPSLATE;
    public final RegistryObject<Item> ITEM_BLOCK_ORE_DEEPSLATE;

    public final RegistryObject<Item> ITEM_RAW;

    public final RegistryObject<Block> BLOCK_RAW;
    public final RegistryObject<Item> ITEM_RAW_BLOCK;

    public final RegistryObject<Item> ITEM_NUGGET;
    public final RegistryObject<Item> ITEM_INGOT;
    public final RegistryObject<Block> BLOCK;
    public final RegistryObject<Item> ITEM_BLOCK;

    Metals(final String name, Color tint, final int clusterSize) {
        this(name, name, tint, tint, clusterSize);
    }

    Metals(final String name, Color oreTint, Color metalTint, final int clusterSize) {
        this(name, name, oreTint, metalTint, clusterSize);
    }

    Metals(final String oreName, final String metalName, Color tint, final int clusterSize) {
        this(oreName, metalName, tint, tint, clusterSize);
    }

    Metals(final String oreName, final String metalName, Color oreTint, Color metalTint, final int clusterSize) {
        this.oreName = oreName;
        this.metalName = metalName;
        this.oreTint = oreTint;
        this.metalTint = metalTint;
        this.clusterSize = clusterSize;

        final String oreRegistryName = oreName.toLowerCase();
        final String metalRegistryName = metalName.toLowerCase();

        BLOCK_ORE = BLOCK_REGISTRY.register(format("%s_ore", oreRegistryName), buildBlockOre());
        ITEM_BLOCK_ORE = ITEM_REGISTRY.register(format("%s_ore", oreRegistryName), buildItemOreBlock(BLOCK_ORE));

        BLOCK_ORE_DEEPSLATE = BLOCK_REGISTRY.register(format("%s_ore_deepslate", oreRegistryName), buildBlockOreDeepslate());
        ITEM_BLOCK_ORE_DEEPSLATE = ITEM_REGISTRY.register(format("%s_ore_deepslate", oreRegistryName), buildItemOreBlock(BLOCK_ORE_DEEPSLATE));

        ITEM_RAW = ITEM_REGISTRY.register(format("%s_raw", oreRegistryName), buildItemRaw());
        BLOCK_RAW = BLOCK_REGISTRY.register(format("%s_raw_block", oreRegistryName), buildRawBlock());
        ITEM_RAW_BLOCK = ITEM_REGISTRY.register(format("%s_raw_block", oreRegistryName), buildItemRawBlock(BLOCK_RAW));

        ITEM_NUGGET = ITEM_REGISTRY.register(format("%s_nugget", metalRegistryName), buildItemNugget());
        ITEM_INGOT = ITEM_REGISTRY.register(format("%s_ingot", metalRegistryName), buildItemIngot());
        BLOCK = BLOCK_REGISTRY.register(format("%s_block", metalRegistryName), buildBlock());
        ITEM_BLOCK = ITEM_REGISTRY.register(format("%s_block", metalRegistryName), buildItemBlock(BLOCK));
    }

}
