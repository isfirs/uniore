package isfirs.uniore.providers;

import mcp.MethodsReturnNonnullByDefault;
import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.item.Item;
import net.minecraft.resources.IResource;
import net.minecraft.resources.ResourcePackType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.data.ExistingFileHelper;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault
public abstract class TextureProvider implements IDataProvider {

    private final DataGenerator generator;
    private final String modid;
    private final ExistingFileHelper existingFileHelper;

    private final Map<ResourceLocation, BufferedImage> images = new HashMap<>();

    public TextureProvider(final DataGenerator generator, final String modid, ExistingFileHelper existingFileHelper) {
        this.generator = generator;
        this.modid = modid;
        this.existingFileHelper = existingFileHelper;
    }

    protected abstract void registerTextures();

    @Override
    public void act(final DirectoryCache cache) throws IOException {
        registerTextures();

        writeImages(cache, images, generator.getOutputFolder());
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }
    }

    private static void writeImages(final DirectoryCache cache, final Map<ResourceLocation, BufferedImage> images, final Path path) throws IOException {
        if (images.isEmpty()) return;

        for (Map.Entry<ResourceLocation, BufferedImage> entry : images.entrySet()) {
            final ResourceLocation loc = entry.getKey();
            final BufferedImage value = entry.getValue();
            //

            final Path imagePath = path.resolve("assets/" + loc.getNamespace() + "/textures/" + loc.getPath() + ".png");

            try (
                  final ByteArrayOutputStream baos = new ByteArrayOutputStream();
                  final OutputStream os = Files.newOutputStream(imagePath, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
                  final OutputStream multiplex = new MultiplexOutputStream(baos, os);
            ) {
                if (!ImageIO.write(value, "png", multiplex))
                    throw new IOException("No writer found");

                cache.recordHash(imagePath, IDataProvider.HASH_FUNCTION.hashBytes(baos.toByteArray()).toString());
            }
        }
    }

    protected final void addTintedItemImage(final Supplier<Item> item, BufferedImage baseImage, Color tint, final double intensity) {
        final BufferedImage temp = new BufferedImage(baseImage.getWidth(), baseImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
        tintImage(baseImage, tint, temp, intensity);
        images.put(texturePath("item", item.get().getRegistryName()), temp);
    }

    protected final void addTintedBlockImage(final Supplier<Block> block, BufferedImage baseImage, Color tint, final double intensity) {
        final BufferedImage temp = new BufferedImage(baseImage.getWidth(), baseImage.getHeight(), BufferedImage.TRANSLUCENT);
        tintImage(baseImage, tint, temp, intensity);
        images.put(texturePath("block", block.get().getRegistryName()), temp);
    }

    private ResourceLocation texturePath(final String directory, final ResourceLocation loc) {
        // texture/[block|item]/[registry.path].png
        return new ResourceLocation(modid, String.format("%s/%s", directory, loc.getPath()));
    }

    private void tintImage(final BufferedImage image, final Color tint, final BufferedImage temp, final double intensity) {
        if (image.getWidth() != temp.getWidth() || image.getHeight() != temp.getHeight())
            throw new IllegalArgumentException();

        final Pixel[][] pixels = Pixel.imageToPixels(image);
        for (int y = 0; y < pixels.length; y++) {
            for (int x = 0; x < pixels[y].length; x++) {
                Pixel pixel = pixels[y][x];
                if (pixel.alpha() == 0)
                    continue; // ignore full transparent pixel

                if (pixel.alpha() == 255)
                    continue; // ignore full transparent pixel

                pixels[y][x] = pixel.addColor(tint, intensity)
                      .opaque();
            }
        }

        for (int y = 0; y < pixels.length; y++) {
            for (int x = 0; x < pixels[y].length; x++) {
                Pixel pixel = pixels[y][x];

                temp.setRGB(x, y, pixel.getARGB());
            }
        }
    }

    protected final BufferedImage loadBaseImage(final ResourceLocation loc) {
        try {
            final IResource resource = existingFileHelper.getResource(loc, ResourcePackType.CLIENT_RESOURCES);
            return ImageIO.read(resource.getInputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getName() {
        return "Texture Provider";
    }

    private static final class Pixel {

        /**
         * Inspiriation from <a href="https://stackoverflow.com/questions/6524196/java-get-pixel-array-from-image">Stack
         * Overflow</a>
         *
         * @param image
         *       The Image
         * @return 2D Array of Pixels
         */
        public static Pixel[][] imageToPixels(final BufferedImage image) {
            final byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
            final int width = image.getWidth();
            final int height = image.getHeight();
            final boolean hasAlphaChannel = image.getAlphaRaster() != null;

            Pixel[][] result = new Pixel[height][width];
            if (hasAlphaChannel) {
                final int pixelLength = 4;
                for (int pixel = 0, row = 0, col = 0; pixel + 3 < pixels.length; pixel += pixelLength) {
                    int alpha = pixels[pixel] & 0xff;
                    int blue = pixels[pixel + 1] & 0xff;
                    int green = pixels[pixel + 2] & 0xff;
                    int red = pixels[pixel + 3] & 0xff;
                    result[row][col] = new Pixel(alpha, red, green, blue);
                    col++;
                    if (col == width) {
                        col = 0;
                        row++;
                    }
                }
            } else {
                final int pixelLength = 3;
                for (int pixel = 0, row = 0, col = 0; pixel + 2 < pixels.length; pixel += pixelLength) {
                    int blue = pixels[pixel] & 0xff;
                    int green = pixels[pixel + 1] & 0xff;
                    int red = pixels[pixel + 2] & 0xff;
                    result[row][col] = new Pixel(255, red, green, blue);
                    col++;
                    if (col == width) {
                        col = 0;
                        row++;
                    }
                }
            }

            return result;
        }

        private final int alpha;
        private final int red;
        private final int green;
        private final int blue;

        public Pixel(final int alpha, final int red, final int green, final int blue) {
            this.alpha = alpha;
            this.red = red;
            this.green = green;
            this.blue = blue;
        }

        public int alpha() {
            return alpha;
        }

        public int red() {
            return red;
        }

        public int green() {
            return green;
        }

        public int blue() {
            return blue;
        }

        public int getARGB() {
            return (alpha << 24) | (red << 16) | (green << 8) | blue;
        }

        @Override
        public String toString() {
            return "Pixel{" +
                  "alpha=" + alpha +
                  ", red=" + red +
                  ", green=" + green +
                  ", blue=" + blue +
                  '}';
        }

        public Pixel addColor(final Color tint, final double intensity) {
            int alpha = this.alpha;
            int red = (clamp((int) ((this.red + tint.getRed() * intensity)), 0, 255));
            int green = (clamp((int) ((this.green + tint.getGreen() * intensity)), 0, 255));
            int blue = (clamp((int) ((this.blue + tint.getBlue() * intensity)), 0, 255));
            return new Pixel(alpha, red, green, blue);
        }

        public Pixel transparent() {
            return new Pixel(0, red, green, blue);
        }

        public Pixel opaque() {
            return new Pixel(255, red, green, blue);
        }

        private static int clamp(int value, int min, int max) {
            return value < min ? min : value > max ? max : value;
        }
    }

    private static final class MultiplexOutputStream extends OutputStream {

        private final List<OutputStream> streams;

        public MultiplexOutputStream(final OutputStream... streams) {
            this(Arrays.asList(streams));
        }

        public MultiplexOutputStream(final List<OutputStream> streams) {
            this.streams = new ArrayList<>(streams.size());
            this.streams.addAll(streams);
        }

        @Override
        public void write(int b) throws IOException {
            for (OutputStream os : streams) {
                os.write(b);
            }
        }

        @Override
        public void write(byte[] b) throws IOException {
            for (OutputStream os : streams) {
                os.write(b);
            }
        }

        @Override
        public void write(byte[] b, int off, int len) throws IOException {
            for (OutputStream os : streams) {
                os.write(b, off, len);
            }
        }

        @Override
        public void flush() throws IOException {
            for (OutputStream os : streams) {
                os.flush();
            }
        }

        @Override
        public void close() throws IOException {
            for (OutputStream os : streams) {
                os.close();
            }
        }
    }
}
