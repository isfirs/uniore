package isfirs.uniore.providers;

import isfirs.uniore.UniOre;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;

@Mod.EventBusSubscriber(modid = UniOre.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class GatherDataEventListener {

    @SubscribeEvent
    public static void onGatherData(GatherDataEvent event) {
        final DataGenerator generator = event.getGenerator();
        if (event.includeServer()) {
            generator.addProvider(new MaterialRecipeProvider(generator));
            generator.addProvider(new MaterialsLootTablesProvider(generator));
        }
        if (event.includeClient()) {
            generator.addProvider(new MaterialTextureProvider(generator, event.getExistingFileHelper()));
            generator.addProvider(new MaterialsBlockStateProvider(generator, event.getExistingFileHelper()));
            generator.addProvider(new MaterialsItemProvider(generator, event.getExistingFileHelper()));
            generator.addProvider(new MaterialsLangProvider(generator));
        }
    }

}
