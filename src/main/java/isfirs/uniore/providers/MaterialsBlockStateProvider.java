package isfirs.uniore.providers;

import isfirs.uniore.Metals;
import isfirs.uniore.UniOre;
import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.fml.RegistryObject;

import java.util.Arrays;
import java.util.function.Consumer;

public class MaterialsBlockStateProvider extends BlockStateProvider {

    public MaterialsBlockStateProvider(final DataGenerator gen, final ExistingFileHelper exFileHelper) {
        super(gen, UniOre.MODID, exFileHelper);
    }

    @Override
    protected void registerStatesAndModels() {
        final Consumer<RegistryObject<Block>> modelAndStateFactory = r -> {
            final ModelFile mf = cubeAll(r.get());
            simpleBlock(r.get());
            simpleBlockItem(r.get(), mf);
        };

        final Consumer<Metals> builder = makeBlockOreModelAndState(modelAndStateFactory)
              .andThen(makeBlockOreDeepslateModelAndState(modelAndStateFactory))
              .andThen(makeBlockRawModelAndState(modelAndStateFactory))
              .andThen(makeBlockModelAndState(modelAndStateFactory))
              .andThen(makeItemsModels());

        //
        Arrays.stream(Metals.values()) //
              .forEach(builder);
    }

    private Consumer<Metals> makeBlockModelAndState(final Consumer<RegistryObject<Block>> modelAndStateFactory) {
        return m -> modelAndStateFactory.accept(m.BLOCK);
    }

    private Consumer<Metals> makeBlockRawModelAndState(final Consumer<RegistryObject<Block>> modelAndStateFactory) {
        return m -> modelAndStateFactory.accept(m.BLOCK_RAW);
    }

    private Consumer<Metals> makeBlockOreDeepslateModelAndState(final Consumer<RegistryObject<Block>> modelAndStateFactory) {
        return m -> modelAndStateFactory.accept(m.BLOCK_ORE_DEEPSLATE);
    }

    private Consumer<Metals> makeBlockOreModelAndState(final Consumer<RegistryObject<Block>> modelAndStateFactory) {
        return m -> modelAndStateFactory.accept(m.BLOCK_ORE);
    }

    private Consumer<Metals> makeItemsModels() {
        return m -> {

        };
    }

    private void test(Metals m) {
    }

}
