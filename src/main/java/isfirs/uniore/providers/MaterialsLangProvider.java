package isfirs.uniore.providers;

import isfirs.uniore.Metals;
import isfirs.uniore.UniOre;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.data.LanguageProvider;

import java.util.Arrays;
import java.util.function.Consumer;

import static java.lang.String.format;

public class MaterialsLangProvider extends LanguageProvider {

    public MaterialsLangProvider(final DataGenerator gen) {
        super(gen, UniOre.MODID, "en_us");
    }

    @Override
    protected void addTranslations() {
        final Consumer<Metals> builder = addBlockOre()
              .andThen(addBlockOreDeepslate())
              .andThen(addItemRaw())
              .andThen(addBlockRaw())
              .andThen(addItemNugget())
              .andThen(addItemIngot())
              .andThen(addBlock());

        Arrays.stream(Metals.values())
              .forEach(builder);
    }

    private Consumer<Metals> addBlockOre() {
        return m -> addBlock(m.BLOCK_ORE, format("%s Ore", m.oreName));
    }

    private Consumer<Metals> addBlockOreDeepslate() {
        return m -> addBlock(m.BLOCK_ORE_DEEPSLATE, format("Deepslate %s Ore", m.oreName));
    }

    private Consumer<Metals> addItemRaw() {
        return m -> addItem(m.ITEM_RAW, format("Raw %s", m.oreName));
    }

    private Consumer<Metals> addBlockRaw() {
        return m -> addBlock(m.BLOCK_RAW, format("Block of Raw %s", m.metalName));
    }

    private Consumer<Metals> addItemNugget() {
        return m -> addItem(m.ITEM_NUGGET, format("%s Nugget", m.metalName));
    }

    private Consumer<Metals> addItemIngot() {
        return m -> addItem(m.ITEM_INGOT, format("%s Ingot", m.metalName));
    }

    private Consumer<Metals> addBlock() {
        return m -> addBlock(m.BLOCK, format("%s Block", m.metalName));
    }

}
