package isfirs.uniore.providers;

import isfirs.uniore.Metals;
import net.minecraft.data.DataGenerator;

import java.util.Arrays;
import java.util.function.Consumer;

public class MaterialsLootTablesProvider extends BaseLootTableProvider {

    public MaterialsLootTablesProvider(final DataGenerator dataGeneratorIn) {
        super(dataGeneratorIn);
    }

    @Override
    protected void addTables() {
        final Consumer<Metals> builder = makeBlockLootTable()
              .andThen(makeBlockOreRawLootTable())
              .andThen(makeOreLootTable())
              .andThen(makeOreDeepslateLootTable());

        //
        Arrays.stream(Metals.values())
              .forEach(builder);
    }

    private Consumer<Metals> makeBlockLootTable() {
        return m -> lootTables.put(m.BLOCK.get(), createStandardBlockEntry(m.BLOCK.get()));
    }

    private Consumer<Metals> makeBlockOreRawLootTable() {
        return m -> lootTables.put(m.BLOCK_RAW.get(), createStandardBlockEntry(m.BLOCK_RAW.get()));
    }

    private Consumer<Metals> makeOreLootTable() {
        return m -> lootTables.put(m.BLOCK_ORE.get(), createOreEntry(m.BLOCK_ORE.get(), m.ITEM_RAW.get()));
    }

    private Consumer<Metals> makeOreDeepslateLootTable() {
        return m -> lootTables.put(m.BLOCK_ORE_DEEPSLATE.get(), createOreEntry(m.BLOCK_ORE_DEEPSLATE.get(), m.ITEM_RAW.get()));
    }

}
