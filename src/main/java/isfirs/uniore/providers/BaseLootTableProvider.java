package isfirs.uniore.providers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.minecraft.advancements.criterion.EnchantmentPredicate;
import net.minecraft.advancements.criterion.ItemPredicate;
import net.minecraft.advancements.criterion.MinMaxBounds;
import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.data.LootTableProvider;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.item.Item;
import net.minecraft.loot.AlternativesLootEntry;
import net.minecraft.loot.ConstantRange;
import net.minecraft.loot.DynamicLootEntry;
import net.minecraft.loot.ItemLootEntry;
import net.minecraft.loot.LootEntry;
import net.minecraft.loot.LootParameterSets;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.LootTableManager;
import net.minecraft.loot.conditions.Alternative;
import net.minecraft.loot.conditions.MatchTool;
import net.minecraft.loot.conditions.SurvivesExplosion;
import net.minecraft.loot.functions.ApplyBonus;
import net.minecraft.loot.functions.CopyName;
import net.minecraft.loot.functions.CopyNbt;
import net.minecraft.loot.functions.SetContents;
import net.minecraft.util.ResourceLocation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

/**
 * Source: https://github.com/McJty/YouTubeModding14/blob/fef3d4d5131abc78ca62b9ba66b7fa7962f31bd7/src/main/java/com/mcjty/mytutorial/datagen/BaseLootTableProvider.java
 */

abstract class BaseLootTableProvider extends LootTableProvider {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

    protected final Map<Block, LootTable.Builder> lootTables = new HashMap<>();
    private final DataGenerator generator;

    public BaseLootTableProvider(DataGenerator dataGeneratorIn) {
        super(dataGeneratorIn);
        this.generator = dataGeneratorIn;
    }

    protected abstract void addTables();

    protected LootTable.Builder createOreEntry(Block block, Item rawOre) {
        LootPool.Builder builder = LootPool.builder()
              .rolls(ConstantRange.of(1))
              .bonusRolls(0, 0)
              .addEntry(
                    AlternativesLootEntry.builder(
                          matchToolEntry(block),
                          dropOreRaw(rawOre)
                    )
              );
        return LootTable.builder().addLootPool(builder);
    }

    private LootEntry.Builder<?> matchToolEntry(Block block) {
        return ItemLootEntry.builder(block)
              .acceptCondition(
                    MatchTool.builder(
                          ItemPredicate.Builder.create()
                                .enchantment(
                                      new EnchantmentPredicate(Enchantments.SILK_TOUCH, MinMaxBounds.IntBound.atLeast(1))
                                )
                    )
              );
    }

    private LootEntry.Builder<?> dropOreRaw(Item oreRaw) {
        return ItemLootEntry.builder(oreRaw)
              .acceptFunction(ApplyBonus.oreDrops(Enchantments.FORTUNE));

    }

    protected LootTable.Builder createStandardBlockEntry(Block block) {
        LootPool.Builder builder = LootPool.builder()
              .rolls(ConstantRange.of(1))
              .bonusRolls(0, 0)
              .acceptCondition(SurvivesExplosion.builder())
              .addEntry(
                    ItemLootEntry.builder(block)
              );
        return LootTable.builder().addLootPool(builder);
    }

    @Override
    public void act(DirectoryCache cache) {
        addTables();

        Map<ResourceLocation, LootTable> tables = new HashMap<>();
        for (Map.Entry<Block, LootTable.Builder> entry : lootTables.entrySet()) {
            tables.put(entry.getKey().getLootTable(), entry.getValue().setParameterSet(LootParameterSets.BLOCK).build());
        }
        writeTables(cache, tables);
    }

    private void writeTables(DirectoryCache cache, Map<ResourceLocation, LootTable> tables) {
        Path outputFolder = this.generator.getOutputFolder();
        tables.forEach((key, lootTable) -> {
            Path path = outputFolder.resolve("data/" + key.getNamespace() + "/loot_tables/" + key.getPath() + ".json");
            try {
                IDataProvider.save(GSON, cache, LootTableManager.toJson(lootTable), path);
            } catch (IOException e) {
                LOGGER.error("Couldn't write loot table {}", path, e);
            }
        });
    }

}
