package isfirs.uniore.providers;

import isfirs.uniore.Metals;
import isfirs.uniore.UniOre;
import net.minecraft.data.DataGenerator;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.fml.RegistryObject;

import java.util.Arrays;
import java.util.function.Consumer;

public class MaterialsItemProvider extends ItemModelProvider {

    public MaterialsItemProvider(DataGenerator generator, ExistingFileHelper existingFileHelper) {
        super(generator, UniOre.MODID, existingFileHelper);
    }

    private static ResourceLocation itemTexture(final RegistryObject<Item> item) {
        final ResourceLocation registryName = item.get().getRegistryName();
        return new ResourceLocation(registryName.getNamespace(), String.format("item/%s", registryName.getPath()));
    }

    @Override
    protected void registerModels() {
        final Consumer<RegistryObject<Item>> modelFactory = i -> getBuilder(String.format("item/%s", i.get().getRegistryName().getPath()))
              .parent(new ModelFile.UncheckedModelFile("item/generated"))
              .texture("layer0", itemTexture(i));

        final Consumer<Metals> builder = makeNuggetModel(modelFactory)
              .andThen(makeIngotModel(modelFactory))
              .andThen(makeRawModel(modelFactory));

        //
        Arrays.stream(Metals.values()) //
              .forEach(builder);
    }

    private Consumer<Metals> makeNuggetModel(final Consumer<RegistryObject<Item>> modelFactory) {
        return m -> modelFactory.accept(m.ITEM_NUGGET);
    }

    private Consumer<Metals> makeIngotModel(final Consumer<RegistryObject<Item>> modelFactory) {
        return m -> modelFactory.accept(m.ITEM_INGOT);
    }

    private Consumer<Metals> makeRawModel(final Consumer<RegistryObject<Item>> modelFactory) {
        return m -> modelFactory.accept(m.ITEM_RAW);
    }

}
