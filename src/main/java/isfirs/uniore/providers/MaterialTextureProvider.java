package isfirs.uniore.providers;

import isfirs.uniore.Metals;
import isfirs.uniore.UniOre;
import net.minecraft.data.DataGenerator;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.data.ExistingFileHelper;

import java.util.Arrays;

public class MaterialTextureProvider extends TextureProvider {

    private static final ResourceLocation RES_BASE_BLOCK_ORE = new ResourceLocation(UniOre.MODID, "textures/base/block/ore.png");
    private static final ResourceLocation RES_BASE_BLOCK_ORE_DEEPSLATE = new ResourceLocation(UniOre.MODID, "textures/base/block/ore_deepslate.png");
    private static final ResourceLocation RES_BASE_ITEM_RAW = new ResourceLocation(UniOre.MODID, "textures/base/item/raw.png");
    private static final ResourceLocation RES_BASE_BLOCK_RAW = new ResourceLocation(UniOre.MODID, "textures/base/block/raw_block.png");
    private static final ResourceLocation RES_BASE_ITEM_NUGGET = new ResourceLocation(UniOre.MODID, "textures/base/item/nugget.png");
    private static final ResourceLocation RES_BASE_ITEM_INGOT = new ResourceLocation(UniOre.MODID, "textures/base/item/ingot.png");
    private static final ResourceLocation RES_BASE_BLOCK = new ResourceLocation(UniOre.MODID, "textures/base/block/block.png");

    public MaterialTextureProvider(final DataGenerator generator, ExistingFileHelper existingFileHelper) {
        super(generator, UniOre.MODID, existingFileHelper);
    }

    @Override
    protected void registerTextures()  {
        Arrays.stream(Metals.values()) //
              .forEach(m -> {
                  addTintedBlockImage(m.BLOCK_ORE, loadBaseImage(RES_BASE_BLOCK_ORE), m.oreTint, .6);
                  addTintedBlockImage(m.BLOCK_ORE_DEEPSLATE, loadBaseImage(RES_BASE_BLOCK_ORE_DEEPSLATE), m.oreTint, .6);
                  addTintedItemImage(m.ITEM_RAW, loadBaseImage(RES_BASE_ITEM_RAW), m.oreTint, .6);
                  addTintedBlockImage(m.BLOCK_RAW, loadBaseImage(RES_BASE_BLOCK_RAW), m.oreTint, .6);
                  addTintedItemImage(m.ITEM_NUGGET, loadBaseImage(RES_BASE_ITEM_NUGGET), m.metalTint, .3);
                  addTintedItemImage(m.ITEM_INGOT, loadBaseImage(RES_BASE_ITEM_INGOT), m.metalTint, .5);
                  addTintedBlockImage(m.BLOCK, loadBaseImage(RES_BASE_BLOCK), m.metalTint, .3);
              });
    }
}
