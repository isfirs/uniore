package isfirs.uniore.providers;

import isfirs.uniore.Metals;
import isfirs.uniore.UniOre;
import net.minecraft.advancements.criterion.InventoryChangeTrigger;
import net.minecraft.data.CookingRecipeBuilder;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.IFinishedRecipe;
import net.minecraft.data.RecipeProvider;
import net.minecraft.data.ShapedRecipeBuilder;
import net.minecraft.data.ShapelessRecipeBuilder;
import net.minecraft.item.Item;
import net.minecraft.item.crafting.Ingredient;
import net.minecraftforge.fml.RegistryObject;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Arrays;
import java.util.function.Consumer;

@ParametersAreNonnullByDefault
public class MaterialRecipeProvider extends RecipeProvider {
    public MaterialRecipeProvider(final DataGenerator generator) {
        super(generator);
    }

    @Override
    protected void registerRecipes(final Consumer<IFinishedRecipe> consumer) {
        Consumer<Metals> builder = m -> {
        };

        builder = builder.andThen(m -> shapedRawOreBlock(consumer, m))
              .andThen(m -> shapelessRawOre(consumer, m))
              .andThen(m -> shapedNuggetToIngot(consumer, m))
              .andThen(m -> shapelessIngotToNugget(consumer, m))
              .andThen(m -> shapedIngotToBlock(consumer, m))
              .andThen(m -> shapelessBlockToIngot(consumer, m))
              .andThen(m -> blastOreToIngot(consumer, m))
              .andThen(m -> blastRawOreToIngot(consumer, m))
              .andThen(m -> smeltOreToIngot(consumer, m))
              .andThen(m -> smeltRawOreToIngot(consumer, m));

        //
        Arrays.stream(Metals.values()) //
              .forEach(builder);
    }

    private static void shapedRawOreBlock(final Consumer<IFinishedRecipe> consumer, final Metals materials) {
        shaped3x3Recipe(consumer, materials.ITEM_RAW, materials.ITEM_RAW_BLOCK);
    }

    private static void shapelessRawOre(final Consumer<IFinishedRecipe> consumer, final Metals materials) {
        shapeless(consumer, materials.ITEM_RAW_BLOCK, materials.ITEM_RAW);
    }

    private static void shapedNuggetToIngot(final Consumer<IFinishedRecipe> consumer, final Metals materials) {
        shaped3x3Recipe(consumer, materials.ITEM_NUGGET, materials.ITEM_INGOT);
    }

    private static void shapelessIngotToNugget(final Consumer<IFinishedRecipe> consumer, final Metals materials) {
        shapeless(consumer, materials.ITEM_INGOT, materials.ITEM_NUGGET);
    }

    private static void shapedIngotToBlock(final Consumer<IFinishedRecipe> consumer, final Metals materials) {
        shaped3x3Recipe(consumer, materials.ITEM_INGOT, materials.ITEM_BLOCK);
    }

    private static void shapelessBlockToIngot(final Consumer<IFinishedRecipe> consumer, final Metals materials) {
        shapeless(consumer, materials.ITEM_BLOCK, materials.ITEM_INGOT);
    }

    private static void blastOreToIngot(final Consumer<IFinishedRecipe> consumer, final Metals materials) {
        blastingRecipe(consumer, materials.ITEM_BLOCK_ORE, materials.ITEM_INGOT);
    }

    private static void blastRawOreToIngot(final Consumer<IFinishedRecipe> consumer, final Metals materials) {
        blastingRecipe(consumer, materials.ITEM_RAW, materials.ITEM_INGOT);
    }
    private static void smeltOreToIngot(final Consumer<IFinishedRecipe> consumer, final Metals materials) {
        smeltingRecipe(consumer, materials.ITEM_BLOCK_ORE, materials.ITEM_INGOT);

    }
    private static void smeltRawOreToIngot(final Consumer<IFinishedRecipe> consumer, final Metals materials) {
        smeltingRecipe(consumer, materials.ITEM_RAW, materials.ITEM_INGOT);
    }

    // ---

    private static void blastingRecipe(final Consumer<IFinishedRecipe> consumer, final RegistryObject<Item> input, final RegistryObject<Item> output) {
        CookingRecipeBuilder.blastingRecipe(Ingredient.fromItems(input.get()), output.get(), .1f, 100)
              .addCriterion(output.get().getRegistryName().getPath(), InventoryChangeTrigger.Instance.forItems(input.get()))
              .build(consumer, makeBlastingRecipeNameFromItems(input, output));
    }

    private static void smeltingRecipe(final Consumer<IFinishedRecipe> consumer, final RegistryObject<Item> input, final RegistryObject<Item> output) {
        CookingRecipeBuilder.smeltingRecipe(Ingredient.fromItems(input.get()), output.get(), .1f, 200)
              .addCriterion(output.get().getRegistryName().getPath(), InventoryChangeTrigger.Instance.forItems(input.get()))
              .build(consumer, makeSmeltingRecipeNameFromItems(input, output));
    }

    private static void shaped3x3Recipe(final Consumer<IFinishedRecipe> consumer, final RegistryObject<Item> input, final RegistryObject<Item> output) {
        ShapedRecipeBuilder.shapedRecipe(output.get())
              .patternLine("###")
              .patternLine("###")
              .patternLine("###")
              .key('#', input.get())
              .setGroup(UniOre.MODID)
              .addCriterion(input.get().getRegistryName().getPath(), InventoryChangeTrigger.Instance.forItems(input.get()))
              .build(consumer, makeRecipeNameFromItems(input, output));
    }

    private static void shapeless(final Consumer<IFinishedRecipe> consumer, final RegistryObject<Item> input, final RegistryObject<Item> output) {
        shapeless(consumer, input, output, 9);
    }

    private static void shapeless(final Consumer<IFinishedRecipe> consumer, final RegistryObject<Item> input, final RegistryObject<Item> output, final int amount) {
        ShapelessRecipeBuilder.shapelessRecipe(output.get(), amount)
              .addIngredient(input.get())
              .setGroup(UniOre.MODID)
              .addCriterion(input.get().getRegistryName().getPath(), InventoryChangeTrigger.Instance.forItems(input.get()))
              .build(consumer, makeRecipeNameFromItems(input, output));
    }

    private static String makeRecipeNameFromItems(final RegistryObject<Item> input, final RegistryObject<Item> output) {
        return String.format("%s:%s_to_%s", UniOre.MODID, input.get().getRegistryName().getPath(), output.get().getRegistryName().getPath());
    }

    private static String makeBlastingRecipeNameFromItems(final RegistryObject<Item> input, final RegistryObject<Item> output) {
        return String.format("%s:%s_to_%s_by_blasting", UniOre.MODID, input.get().getRegistryName().getPath(), output.get().getRegistryName().getPath());
    }

    private static String makeSmeltingRecipeNameFromItems(final RegistryObject<Item> input, final RegistryObject<Item> output) {
        return String.format("%s:%s_to_%s_by_smelting", UniOre.MODID, input.get().getRegistryName().getPath(), output.get().getRegistryName().getPath());
    }

}
