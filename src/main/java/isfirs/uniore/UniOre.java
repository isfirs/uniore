package isfirs.uniore;

import com.google.common.base.Supplier;
import isfirs.uniore.events.BiomeLoadingEventListener;
import net.minecraft.block.Block;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.IFeatureConfig;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraft.world.gen.placement.TopSolidRangeConfig;
import net.minecraftforge.common.world.BiomeGenerationSettingsBuilder;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Mod(UniOre.MODID)
public class UniOre {

    // Directly reference a log4j logger.
    private static final Logger LOGGER = LogManager.getLogger();

    // The value here should match an entry in the META-INF/mods.toml file
    public static final String MODID = "uniore";

    public static ResourceLocation makeId(final String path) {
        return new ResourceLocation(MODID, path);
    }

    public UniOre() {
        Registration.init();

        IEventBus modEventBus = FMLJavaModLoadingContext.get().getModEventBus();
        modEventBus.addListener(this::onSetup);
    }

    public void onSetup(FMLCommonSetupEvent event) {
        event.enqueueWork(UniOre::setupFeatures);
    }

    private static void setupFeatures() {
        Arrays.stream(Metals.values()) //
              .forEach(metal -> {
                  final Block oreBlock = metal.BLOCK_ORE.get();
                  final ConfiguredFeature<?, ?> fc = WorldGenRegistries.register(
                        WorldGenRegistries.CONFIGURED_FEATURE,
                        oreBlock.getRegistryName(),
                        Feature.ORE
                              .withConfiguration(
                                    new OreFeatureConfig(
                                          OreFeatureConfig.FillerBlockType.BASE_STONE_OVERWORLD,
                                          oreBlock.getDefaultState(),
                                          8
                                    )
                              )
                              .range(64)
                              .square()
                              .count(metal.clusterSize)
                              .withPlacement(
                                    Placement.RANGE
                                          .configure(
                                                new TopSolidRangeConfig(12, 12, 72)
                                          )
                                          .square()
                                          .count(metal.clusterSize)
                              )
                  );

                  BiomeLoadingEventListener.add(fc);
              });
    }

}
