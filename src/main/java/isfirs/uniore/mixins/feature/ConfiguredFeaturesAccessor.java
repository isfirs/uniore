package isfirs.uniore.mixins.feature;

import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Features;
import net.minecraft.world.gen.feature.IFeatureConfig;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(Features.class)
public interface ConfiguredFeaturesAccessor {

    @Invoker("register")
    static <FC extends IFeatureConfig> ConfiguredFeature<FC, ?> register(String id,
                                                                         ConfiguredFeature<FC, ?> configuredFeature) {
        throw new AssertionError();
    }

}
