# UniOre
> An attempt to unify materials

## Auto-Update
See [Forge Docs](https://mcforge.readthedocs.io/en/latest/gettingstarted/autoupdate/)

## Materials

### Single Material Ores

- [x] Tin
  - Cassiterite
- [x] Aluminum
  - Bauxite
- [x] Silver
- [x] Titanium
- [x] Tungsten
- [x] Zinc

### Duo Ores

- [ ] Uranium (235, 238)

### Subsystems

- DataGen
    - [x] Blockstates
    - [x] Block Models
    - [x] Item Models
    - [x] Recipes
        - [x] 9 Nugget -> 1 Ingot
        - [x] 1 Ingot -> 9 Nugget
        - [x] 9 Ingot -> 1 Block
        - [x] 1 Block -> 9 Ingot
        - [x] 9 Raw -> 1 Raw Block
        - [x] 1 Raw Block -> 9 Raw
    - [x] Advancements for Recipes
    - [x] Loot tables to pick up blocks
    - [x] Language file en_us
    - [ ] Tags
    - [x] World generation

## Gems

- [ ] Saphhire
- [ ] Rubin
- [ ] Topaz

### Subsystems

- Data Gen
    - [ ] Blockstates
    - [ ] Block Models
    - [ ] Item Models
    - [ ] Recipes
      - TODO
    - [ ] Advancements for Recipes
    - [ ] Loot tables to pick up blocks
    - [ ] Language file en_us
    - [ ] Tags
    - [ ] World generation

## Alloys

- [ ] Bronze
- [ ] Steel
- [ ] Electrum

### Subsystems

- Data Gen
    - [ ] Blockstates
    - [ ] Block Models
    - [ ] Item Models
    - [ ] Recipes
        - TODO
    - [ ] Advancements for Recipes
    - [ ] Loot tables to pick up blocks
    - [ ] Language file en_us
    - [ ] Tags
